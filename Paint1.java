package paint;

import java.util.logging.Logger;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 *
 * @author Deven
 */
public class Paint extends Application {

    ImageView myImageView;

    private File selected;

    @Override
    public void start(Stage primaryStage) {

        //creates window to open file finder with a load button
        Button btnLoad = new Button("");

        //Scene scene = new Scene(rootBox, 300, 300);
        // set title for the stage 
        primaryStage.setTitle("creating MenuBar");

        // create a menu 
        Menu m = new Menu("Menu");

        // create menuitems 
        MenuItem m1 = new MenuItem("Load");
        MenuItem m2 = new MenuItem("Save");

        // makes you choose a file when you press "load"
        m1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent LoadPic) {

                FileChooser fileChooser = new FileChooser();

                //Set extension filter
                FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);

                //Show open file dialog
                selected = fileChooser.showOpenDialog(null);

                try {
                    BufferedImage bufferedImage = ImageIO.read(selected);
                    Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                    myImageView.setImage(image);
                } catch (IOException ex) {
                    Logger.getLogger(Paint.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        myImageView = new ImageView();

        VBox rootBox = new VBox();

        // add menu items to menu 
        m.getItems().add(m1);
        m.getItems().add(m2);

        Stage secondStage = new Stage();

        ImageView imageView = new ImageView();

        //saves a selected image when you hit "save"
        m2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save Image");

                selected = fileChooser.showSaveDialog(secondStage);
                if (selected != null) {
                    try {
                        ImageIO.write(SwingFXUtils.fromFXImage(imageView.getImage(),
                                null), "png", selected);
                    } catch (IOException ex) {
                        Logger.getLogger(
                                Paint.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        // label to display events 
        Label l = new Label("\t\t\t\t"
                + " ");

        // create a menubar 
        MenuBar mb = new MenuBar();

        // add menu to menubar 
        mb.getMenus().add(m);

        // create a VBox 
        VBox vb = new VBox(mb, l);
        vb.getChildren().addAll(myImageView);

        // create a scene 
        Scene sc = new Scene(vb, 500, 300);

        // set the scene 
        primaryStage.setScene(sc);

        primaryStage.show();

        primaryStage.setTitle("Pain(t)");
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
