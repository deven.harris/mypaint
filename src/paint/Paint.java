package paint;

import javafx.scene.control.Slider;
import javafx.scene.shape.Shape;
import javafx.scene.paint.Color;
import java.util.logging.Logger;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.scene.canvas.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Line;
import java.util.Stack;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Deven
 */
public class Paint extends Application {

    //initializations
    ImageView myImageView;
    private File selected;

    private String filepath;

    private int width = 1200;
    private int height = 800;
    private final Canvas canvas = new Canvas(width, height);

    @Override
    public void start(Stage primaryStage) {

        GraphicsContext gc;
        gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(1);
        //creates window to open file finder with a load button
        Button btnLoad = new Button("");

        // set title for the stage 
        primaryStage.setTitle("creating MenuBar");

        // create a menu 
        Menu m = new Menu("Menu");

        MenuItem m1 = new MenuItem("Load");
        MenuItem m2 = new MenuItem("Save");
        MenuItem m3 = new MenuItem("Save As");
        MenuItem m4 = new MenuItem("Help");

        // makes you choose a file when you press "load"
        m1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent LoadPic) {

                FileChooser fileChooser = new FileChooser();

                //Set extension filter
                FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);

                //Show open file dialog
                selected = fileChooser.showOpenDialog(null);

                try {
                    BufferedImage bufferedImage = ImageIO.read(selected);
                    Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                    canvas.setHeight(image.getHeight());
                    canvas.setWidth(image.getWidth());
                    gc.drawImage(image, 0, 0);

                } catch (IOException ex) {
                    Logger.getLogger(Paint.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        myImageView = new ImageView();

        VBox rootBox = new VBox();

        // add menu items to menu 
        m.getItems().add(m1);
        m.getItems().add(m2);
        m.getItems().add(m3);
        m.getItems().add(m4);

        Stage secondStage = new Stage();
        Stage thirdStage = new Stage();

        ImageView imageView = new ImageView();

        //selects an image and saves it when you hit "save"
        m2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                File file = new File(filepath);
                if (file != null) {
                    try {
                        WritableImage wImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
                        canvas.snapshot(null, wImage);
                        RenderedImage rImage = SwingFXUtils.fromFXImage(wImage, null);
                        ImageIO.write(rImage, "png", file);
                    } catch (IOException ex) {
                        Logger.getLogger(Paint.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        //take a loaded image and save as a certain file name where you want
        m3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent a) {
                FileChooser choose = new FileChooser();
                choose.setTitle("Save As");
                choose.getExtensionFilters().add(new ExtensionFilter("Images", "*.png", "*.jpg"));
                File file = choose.showSaveDialog(primaryStage);
                if (file != null) {
                    try {
                        WritableImage wImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
                        canvas.snapshot(null, wImage);
                        RenderedImage rImage = SwingFXUtils.fromFXImage(wImage, null);
                        ImageIO.write(rImage, "png", file);
                    } catch (IOException ex) {
                        Logger.getLogger(Paint.class.getName()).log(Level.SEVERE, null, ex);

                    }
                }
                filepath = file.getPath();
            }
        });

        m4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                Label secondLabel = new Label("Version 2.1");

                StackPane secondaryLayout = new StackPane();
                secondaryLayout.getChildren().add(secondLabel);

                Scene secondScene = new Scene(secondaryLayout, 200, 100);

                Stage secondStage = new Stage();
                secondStage.setTitle("Second Stage");
                secondStage.setScene(secondScene);

                //Set position of second window, related to primary window.
                secondStage.setX(primaryStage.getX() + 250);
                secondStage.setY(primaryStage.getY() + 100);

                secondStage.show();
            }
        });

        //create scroll bars
        ScrollPane sp = new ScrollPane();
        sp.setContent(canvas);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        //create ability to draw line
        Stack<Shape> undoHistory = new Stack();
        Stack<Shape> redoHistory = new Stack();
        ToggleButton linebtn = new ToggleButton("Line");
        ToggleButton[] toolsArr = {linebtn};
        ToggleGroup tools = new ToggleGroup();

        for (ToggleButton tool : toolsArr) {
            tool.setMinWidth(90);
            tool.setToggleGroup(tools);
            tool.setCursor(Cursor.HAND);
        }

        ColorPicker cpLine = new ColorPicker(Color.BLACK);
        ColorPicker cpFill = new ColorPicker(Color.TRANSPARENT);

        Label line_color = new Label("Line Color");
        Label line_width = new Label("3.0");
        Slider slider = new Slider(1, 50, 3);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        GraphicsContext gc2;
        gc2 = canvas.getGraphicsContext2D();
        gc.setLineWidth(1);

        Line line = new Line();

        canvas.setOnMousePressed(e -> {
            if (linebtn.isSelected()) {
                gc.setStroke(cpLine.getValue());
                line.setStartX(e.getX());
                line.setStartY(e.getY());
            }
        });

        canvas.setOnMouseReleased(e -> {
            if (linebtn.isSelected()) {
                line.setEndX(e.getX());
                line.setEndY(e.getY());
                gc.strokeLine(line.getStartX(), line.getStartY(), line.getEndX(), line.getEndY());

                undoHistory.push(new Line(line.getStartX(), line.getStartY(), line.getEndX(), line.getEndY()));
            }
            redoHistory.clear();
            Shape lastUndo = undoHistory.lastElement();
            lastUndo.setFill(gc.getFill());
            lastUndo.setStroke(gc.getStroke());
            lastUndo.setStrokeWidth(gc.getLineWidth());
        });

        //color picker
        cpLine.setOnAction(e -> {
            gc.setStroke(cpLine.getValue());
        });
        cpFill.setOnAction(e -> {
            gc.setFill(cpFill.getValue());
        });

        //slider
        slider.valueProperty().addListener(e -> {
            double width = slider.getValue();
            line_width.setText(String.format("%.1f", width));
            gc.setLineWidth(width);
        });

        // label to display events 
        Label l = new Label("\t\t\t\t"
                + " ");

        // create a menubar 
        MenuBar mb = new MenuBar();
        mb.getMenus().add(m);

        // create a VBox 
        VBox vb = new VBox(mb, l);
        vb.getChildren().addAll(canvas);
        vb.getChildren().addAll(linebtn, line_color, cpLine, line_width, slider);

        BorderPane pane = new BorderPane();
        pane.setLeft(vb);
        pane.setCenter(sp);

        // create a scene
        Scene sc = new Scene(pane, canvas.getWidth(), canvas.getHeight());
        primaryStage.setScene(sc);

        primaryStage.show();

        primaryStage.setTitle("Pain(t)");
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
